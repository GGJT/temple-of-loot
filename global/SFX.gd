extends Node

"""
	SFX
	Global non-positional sfx player
"""

# Play a loaded sound effect
func play( sound ):
	var player = AudioStreamPlayer.new()
	add_child(player)
	player.stream = sound
	# node kills itself after it's done
	player.connect("finished", self, "queue_free()")
	player.play()
