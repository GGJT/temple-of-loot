extends Node

var DEV_MODE = true

var TITLE_SCENE = preload("res://scenes/Title.tscn")
var GAME_SCENE = preload("res://scenes/Game.tscn")
var GAME_OVER_SCENE = preload("res://scenes/GameOver.tscn")
var VICTORY_SCENE = preload("res://scenes/Victory.tscn")

# The Main Scene: defined by Main.tscn
var main

# The Player Scene: defined by Player.tscn
var player

signal max_health_changed( to )
signal health_changed( to )
signal score_changed( to )
signal kill_player()

signal win_game()

signal god_mode_changed( to )

var max_health = 4 setget _set_max_health
var health = 4 setget _set_health

var score = 0 setget _set_score

var lava_time = 0.0

var game_time = 0.0

var god_mode = false setget _set_god_mode

func score_points( value ):
	self.score += value


# Change the "current_scene"
func change_scene( to_scene ):
	if !main:
		return
	
	if (main.current_scene):
		main.current_scene.queue_free()
	var new_scene = to_scene.instance()
	main.add_child( new_scene )
	main.current_scene = new_scene




func quit_game():
	# Put pre-quit stuff here!
	print("GOODNIGHT, WORLD!")
	get_tree().quit()



func _ready():
	#Bootstrap the UI
	self.max_health = self.max_health
	self.health = self.health
	self.score = self.score




func _set_max_health( to ):
	max_health = to
	emit_signal("max_health_changed", max_health)



func _set_health( to ):
	health = clamp(to, 0, self.max_health)
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("kill_player")
	
	
func _set_score( to ):
	var earned = to-score
	score = to
	emit_signal( "score_changed", score )
	if main and player and earned > 0:
		var bub = preload("res://scenes/ScoreBubble.tscn").instance()
		main.add_child(bub)
		bub.rect_position = player.position
		bub.start(earned)
	
	
func _set_god_mode( to ):
	if DEV_MODE:
		god_mode = to
		emit_signal( "god_mode_changed", god_mode )
