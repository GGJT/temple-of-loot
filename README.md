This project is was created as a submission for the Godot Gamejam June 2018. It has been released under the MIT license. See LICENSE for details.


CREDITS
------------------------------------------------------
* Ian Williams (YeOldeDM)		(Lead Design & Programming)
* Cory Petkovsek (TinmanJuggernaut)  	(Design, Art & Programming)
* Magnus (Merlin) 			(Sound/Music, Design & Programming) 
* Alexandre Badalo (alex9099) 		(Networking & Programming) 
* Robin Kunimune (Wendigod) 		(Playtesting) 


ATTRIBUTION FOR ASSETS
------------------------------------------------------

__Characters__

Hero:
Jesse M
https://jesse-m.itch.io/jungle-pack    

Fiance:
Niabot, because wikimedia commons lost his roots
You can find him at: http://de.wikipedia.org/wiki/Benutzer:Niabot 
and https://opengameart.org/content/anime-heads
This image was modified from the original by Cory Petkovsek.


__Enemies__

Snake:
Jonathan So		
https://jonathan-so.itch.io/creatorpack

Wasp:
Open Pixel Project
https://openpixelproject.itch.io/opp2017sprites

Bat:
MoikMellah
https://opengameart.org/content/bat-32x32


__Loot__

Kyrise
https://kyrise.itch.io/kyrises-free-16x16-rpg-icon-pack

SorceressGameLab
https://sorceressgamelab.itch.io/rpg-items-part-1   


__Environment__

Lava texture:
LuminousDragonGames
https://opengameart.org/content/2-seamless-lava-tiles

Lava Animations:
ansimuz
https://ansimuz.itch.io/explosion-animations-pack

Mountain Background:
Vicente Nitti
https://vnitti.itch.io/glacial-mountains-parallax-background
This image was modified from the original by Cory Petkovsek.

Font:
Levi Fonts
https://www.urbanfonts.com/fonts/Treasure_Island.font
