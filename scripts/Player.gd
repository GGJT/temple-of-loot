extends KinematicBody2D


var RISING_GRAVITY = 500
var FALLING_GRAVITY = 1200

var MAX_FLOOR_SPEED = 320
var FLOOR_RUN_FORCE = 750
var FLOOR_STOP_FORCE = 1600
var AIR_RUN_FORCE = 100
var AIR_STOP_FORCE = 160
var JUMP_FORCE = 380
var DOUBLEJUMP_FORCE = 240
var JUMP_STOP_FORCE = 1280
var WALLJUMP_FORCE = 300
var WALLSLIDE_FRICTION = 0.3
var COYOTE_TIME = 0.21

var PAIN_FORCE = 2000

#var PUNCH_KICK = 150

var facing = 1 setget _set_facing

var velocity = Vector2()

# Extra velocity given to the player from being "bounced" by an enemy
var pain_velocity = Vector2()

var airtime = 0.0
var double_jumped = false
var jumping = false

var anim = "idle" setget _set_anim

signal triggerAudio_Jump() #signal to Audio node, emit to play jump sound
signal triggerAudio_Pickup()
signal triggerAudio_Dead()
signal triggerAudio_Hurt()

func get_hit( by ):
	if GLOBAL.god_mode:
		return
	# Take Damage
	GLOBAL.health -= 1
	emit_signal("triggerAudio_Hurt")
	# Get Bounced
	var dir = -(by.global_position - global_position).normalized()
	pain_velocity = dir * PAIN_FORCE
	



func _ready():
	GLOBAL.player = self
	GLOBAL.connect("kill_player", self, "_on_kill_player")

func _physics_process(delta):
	
	# god mode processing
	if Input.is_action_just_pressed("GOD"):
		GLOBAL.god_mode = !GLOBAL.god_mode
	
	var G = FALLING_GRAVITY
	if velocity.y < 0:
		G = RISING_GRAVITY
	
	var new_anim = self.anim
	
	var force = Vector2( 0,G  ) + pain_velocity
	
	var U = Input.is_action_pressed("UP")
	var D = Input.is_action_pressed("DOWN")
	var L = Input.is_action_pressed("LEFT")
	var R = Input.is_action_pressed("RIGHT")
	
	if GLOBAL.god_mode && U: 
		velocity.y -= 50
	
	var J = Input.is_action_just_pressed("JUMP")
	var S = Input.is_action_just_pressed("SHOOT")
	
#	if S: #Test the UI
#		GLOBAL.score += 100
#		GLOBAL.health -= 1
#	if J:
#		GLOBAL.health += 1

	var h = int(R) - int(L)
	var v = int(D) - int(U)

	var stop = true
	
	if h <= -1:
		if velocity.x <= MAX_FLOOR_SPEED and velocity.x > -MAX_FLOOR_SPEED:
			force.x -= FLOOR_RUN_FORCE
			stop = false
			if self.facing != -1:
				self.facing = -1
	elif h >= 1:
		if velocity.x >= -MAX_FLOOR_SPEED and velocity.x < MAX_FLOOR_SPEED:
			force.x += FLOOR_RUN_FORCE
			stop = false
			if self.facing != 1:
				self.facing = 1
	
	if stop:
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)
		
		vlen -= FLOOR_STOP_FORCE * delta
		if vlen < 0: vlen = 0
		velocity.x = vlen * vsign

	velocity += force * delta
	if pain_velocity.length() <= 1:
		pain_velocity = Vector2() # Kill pain velocity once it gets tiny
	else:
		pain_velocity *= 0.95 # Bleed off pain velocity over time

		
	var slid =  move_and_slide( velocity, Vector2(0,-1) )
	
	# Set Animation
	new_anim = "idle"
	if is_on_floor() and h != 0:
		new_anim = "run"
	
	if airtime > COYOTE_TIME:
		new_anim = "jump"
	
	if is_on_ceiling() or is_on_floor():
		velocity.y = 0

	if is_on_wall():
		velocity.x = 0
		if airtime > 0:
			velocity *= WALLSLIDE_FRICTION
			new_anim = "grab"

	if is_on_floor():
		airtime = 0
		double_jumped = false
	
	if J and airtime <= COYOTE_TIME or J and is_on_wall():
		emit_signal("triggerAudio_Jump")
		velocity.y = -JUMP_FORCE
		airtime += COYOTE_TIME
		if is_on_wall():
			var n = get_slide_collision( get_slide_count()-1 ).normal
			velocity.x = n.x * WALLJUMP_FORCE

		
	if !Input.is_action_pressed("JUMP") and airtime > COYOTE_TIME and velocity.y < 0:
		velocity.y += JUMP_STOP_FORCE * delta


#	if J and not double_jumped and airtime > COYOTE_TIME:
#		velocity.y = -DOUBLEJUMP_FORCE
#		double_jumped = true
	
	if S:
		pass
		#shoot()?
	
	airtime += delta
	
	
	
	if new_anim != self.anim:
		self.anim = new_anim



func _set_facing( what ):
	facing = what
	$Body.scale.x = facing

func _set_anim( what ):
	anim = what
	$Anim.play(anim)





func _on_kill_player():
	emit_signal("triggerAudio_Dead")
	print("YOU DIED")


func clamp_camera():
	$Camera/Tween.interpolate_property( $Camera, "limit_left", -170, 0, 1.33, Tween.TRANS_SINE, Tween.EASE_IN_OUT )	
	$Camera/Tween.interpolate_property( $Camera, "limit_top", 0, -100000, 20, Tween.TRANS_SINE, Tween.EASE_IN, 0.25 )
	$Camera.offset.y=0
	$Camera/Tween.start()


# When the Player touches a touchable thing!
# (lava, pickups, angry ghosts)
func _on_Hitbox_area_entered(area):
	if area.is_in_group("lava"):
		GLOBAL.emit_signal("kill_player") # tell the world to tell me to kill myself, plzkthx!!!
	
	if area.is_in_group("pickups"):
		emit_signal("triggerAudio_Pickup")
		print("Picked up: ", area.get_name())
		area.pickup()
	
	if area.is_in_group("pot_health"):
		if GLOBAL.health <= GLOBAL.max_health:
			GLOBAL.health += 1
	
	if area.is_in_group("enemies"):
		print("Hey! An enemy touched me: ", area.get_name())
		get_hit( area )

	if area.is_in_group("spikes"):
		GLOBAL.health = 0	# 