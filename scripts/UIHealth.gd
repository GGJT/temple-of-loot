extends HBoxContainer

"""
UI HEALTH HEARTS CONTAINER
"""


func _ready():
	GLOBAL.connect("max_health_changed", self, "_on_max_health_changed")
	GLOBAL.connect("health_changed", self, "_on_health_changed")
	GLOBAL.connect("god_mode_changed", self, "_on_god_mode_changed")


	#Bootstrap the UI
	GLOBAL.max_health = GLOBAL.max_health
	GLOBAL.health = GLOBAL.health
	GLOBAL.score = GLOBAL.score



func _on_max_health_changed( to ):
	for i in range( to-1 ):
		var new_heart = $Heart.duplicate()
		add_child(new_heart)
		new_heart.get_node("Sprite").frame = 0


func _on_health_changed( to ):
	for heart in get_children():
		heart.get_node("Sprite").frame = int( heart.get_position_in_parent()+1 > to )


func _on_god_mode_changed( to ):
	get_parent().get_node("GodMode").visible = to