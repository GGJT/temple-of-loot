extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 0)
const FLOOR_NORMAL = Vector2(0, -1)

const STATE_DEAD = -1
const STATE_SLEEPING = 0
const STATE_MOVING = 1

export (int) var SPEED = 70
export (int) var direction = 1

var linear_velocity = Vector2()
var state = STATE_MOVING

		
func _physics_process(delta):

	if state==STATE_MOVING:
		linear_velocity += GRAVITY_VEC * delta
		linear_velocity.x = direction * SPEED	
		linear_velocity = move_and_slide(linear_velocity, FLOOR_NORMAL)

		if(is_on_wall()):
			if(direction>0):
				direction = -1.0
				$Sprite.flip_h = false if $Sprite.flip_h else true
				#scale = Vector2(direction, 1.0)
			else:
				direction = 1.0
				$Sprite.flip_h = false if $Sprite.flip_h else true
				#scale = Vector2(direction, 1.0)
			
