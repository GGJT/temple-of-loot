extends Node2D

var jump_I = preload("res://assets/audio/FXs/jump/jump_I.wav")
var jump_II = preload("res://assets/audio/FXs/jump/jump_II.wav")
var jump_III = preload("res://assets/audio/FXs/jump/jump_III.wav")
var jump_IV = preload("res://assets/audio/FXs/jump/jump_IV.wav")
var jump_V = preload("res://assets/audio/FXs/jump/jump_V.wav")
var jump_VI = preload("res://assets/audio/FXs/jump/jump_VI.wav")
var jump_VII = preload("res://assets/audio/FXs/jump/jump_VII.wav")
var pickup_I = preload("res://assets/audio/FXs/pickup/pickup_I.wav")
var pickup_II = preload("res://assets/audio/FXs/pickup/pickup_II.wav")
var pickup_III = preload("res://assets/audio/FXs/pickup/pickup_III.wav")
var pickup_IV = preload("res://assets/audio/FXs/pickup/pickup_IV.wav")
var pickup_V = preload("res://assets/audio/FXs/pickup/pickup_V.wav")
var pickup_VI = preload("res://assets/audio/FXs/pickup/pickup_VI.wav")
var pickup_VII = preload("res://assets/audio/FXs/pickup/pickup_VII.wav")
var hurt_I = preload("res://assets/audio/FXs/hurt/hurt_I.wav")
var hurt_II = preload("res://assets/audio/FXs/hurt/hurt_II.wav")
var hurt_III = preload("res://assets/audio/FXs/hurt/hurt_III.wav")
var hurt_IV = preload("res://assets/audio/FXs/hurt/hurt_IV.wav")
var hurt_V = preload("res://assets/audio/FXs/hurt/hurt_V.wav")
var hurt_VI = preload("res://assets/audio/FXs/hurt/hurt_VI.wav")
var hurt_VII = preload("res://assets/audio/FXs/hurt/hurt_VII.wav")


func get_random_number():
    return randi()%6


func _on_Player_triggerAudio_Jump():
	var x = get_random_number()
	match x:
    	0:
        	$FXs/jump.stream = jump_I
    	1:
        	$FXs/jump.stream = jump_II
    	2:
        	$FXs/jump.stream = jump_III
    	3:
        	$FXs/jump.stream = jump_IV
    	4:
        	$FXs/jump.stream = jump_V
    	5:
        	$FXs/jump.stream = jump_VI
    	6:
        	$FXs/jump.stream = jump_VII
	$FXs/jump.play()


func _on_Lava_triggerAudio_lava():
 	$FXs/door_open.play()


func _on_Player_triggerAudio_Pickup():
	var x = get_random_number()
	match x:
    	0:
        	$FXs/pickup.stream = pickup_I
    	1:
        	$FXs/pickup.stream = pickup_II
    	2:
        	$FXs/pickup.stream = pickup_III
    	3:
        	$FXs/pickup.stream = pickup_IV
    	4:
        	$FXs/pickup.stream = pickup_V
    	5:
        	$FXs/pickup.stream = pickup_VI
    	6:
        	$FXs/pickup.stream = pickup_VII
	$FXs/pickup.play()


func _on_Player_triggerAudio_Dead():
	$FXs/dead.play()
	$tracks.stop()


func _on_Player_triggerAudio_Hurt():
	var x = get_random_number()
	match x:
    	0:
        	$FXs/hurt.stream = hurt_I
    	1:
        	$FXs/hurt.stream = hurt_II
    	2:
        	$FXs/hurt.stream = hurt_III
    	3:
        	$FXs/hurt.stream = hurt_IV
    	4:
        	$FXs/hurt.stream = hurt_V
    	5:
        	$FXs/hurt.stream = hurt_VI
    	6:
        	$FXs/hurt.stream = hurt_VII
	$FXs/hurt.play()
