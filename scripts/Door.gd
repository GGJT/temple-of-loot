extends Node2D

func _on_Trigger_Gem_door_closed():
	$Tween.interpolate_property( self, "position", position, position+Vector2(0,128), 1.33, Tween.TRANS_SINE, Tween.EASE_IN_OUT )	
	$Tween.start()
