extends Area2D

export (int) var point_value = 50

func _ready():
	add_to_group("pickups")
	
	
func pickup():
	GLOBAL.score_points(point_value)
	queue_free()
	