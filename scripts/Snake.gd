extends KinematicBody2D

const GRAVITY_VEC = Vector2(0, 900)
const FLOOR_NORMAL = Vector2(0, -1)

const SPEED = 70
const STATE_DEAD = -1
const STATE_SLEEPING = 0
const STATE_MOVING = 1

var linear_velocity = Vector2()
var direction = -1
var anim=""

var state = STATE_MOVING

onready var detect_floor_left = $detect_floor_left
onready var detect_wall_left = $detect_wall_left
onready var detect_floor_right = $detect_floor_right
onready var detect_wall_right = $detect_wall_right
onready var sprite = $Sprite

func _physics_process(delta):
	var new_anim = "idle"

	if state==STATE_MOVING:
		linear_velocity += GRAVITY_VEC * delta
		linear_velocity.x = direction * SPEED
		linear_velocity = move_and_slide(linear_velocity, FLOOR_NORMAL)

		if not detect_floor_left.is_colliding() or detect_wall_left.is_colliding():
			direction = 1.0

		if not detect_floor_right.is_colliding() or detect_wall_right.is_colliding():
			direction = -1.0

		sprite.scale = Vector2(direction, 1.0)
		new_anim = "walk"
	else:
		new_anim = "explode"


	if anim != new_anim:
		anim = new_anim
		$Animation.play(anim)
