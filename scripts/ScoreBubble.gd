extends Label

"""
A little floating point label to visualize you getting points!
"""

func start( value ):
	text = str(value)

func _process(delta):
	rect_position.y -= delta * 3
	modulate.a -= delta


func _on_Timer_timeout():
	queue_free()
