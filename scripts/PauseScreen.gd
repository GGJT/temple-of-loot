extends ColorRect


var paused = false setget _set_paused

func _input( event ):
	if "pressed" in event and event.pressed and !event.is_echo():
		if event.is_action_pressed( "PAUSE" ):
			self.paused = !self.paused


func _set_paused( what ):
	paused = what
	# Actually pauses the game!
	get_tree().paused = paused
	if paused:
		show()	# Show the pause screen
		$PauseOptions/Resume.grab_focus()
	else:	# or not!
		hide()






func _on_Resume_pressed():
	self.paused = false


func _on_Menu_pressed():
	self.paused = false
	GLOBAL.change_scene( GLOBAL.TITLE_SCENE )
	


func _on_Quit_pressed():
	self.paused = false
	GLOBAL.quit_game()
