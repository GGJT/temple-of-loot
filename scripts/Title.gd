extends Control

func _ready():
	$Actions/Start.grab_focus()


func _on_Start_pressed():
	GLOBAL.change_scene( GLOBAL.GAME_SCENE )
#	GLOBAL.main.start()


func _on_Quit_pressed():
	GLOBAL.quit_game()


func _on_Credits_pressed():
	$CreditsPanel.visible = !$CreditsPanel.visible