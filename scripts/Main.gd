extends Node

"""
	MAIN
	
	The Global main_scene of the game.
	The "current scene" being played will be a direct child of this,
	along with other global-level stuff.
"""


var current_scene


func _notification( what ):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		GLOBAL.quit_game()


func _ready():
	randomize()
	GLOBAL.main = self
	# kickstart the Title scene!
	GLOBAL.change_scene( GLOBAL.TITLE_SCENE )


# Nothing else needs to go here



