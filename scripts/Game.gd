extends Node2D

"""
	GAME
	The main game play scene
"""

func _ready():
	GLOBAL.connect("kill_player",self,"_on_kill_player")
	GLOBAL.connect("win_game",self,"_on_win_game")
	
	GLOBAL.health = GLOBAL.max_health
	GLOBAL.score = 0
	
	GLOBAL.lava_time = 0
	GLOBAL.game_time = 0


var game_on = false
func _process(delta):
	if game_on:
		GLOBAL.game_time += delta

func _on_ToTitle_pressed():
	GLOBAL.change_scene( GLOBAL.TITLE_SCENE )


func _on_Quit_pressed():
	GLOBAL.quit_game()



func _on_kill_player():
	# instance the melting player object
	var m = preload("res://scenes/PlayerMelt.tscn").instance()
	
	# replace the real Player with it
	m.position = $Player.position
	$Player.queue_free()
	GLOBAL.player = null
	# Go to Game Over once the melt animation finishes
	m.get_node("Anim").connect("animation_finished", self, "_on_player_death_done" )
	
	add_child(m)


# Called after Player death animation is finished
func _on_player_death_done(anim):
	GLOBAL.change_scene( GLOBAL.GAME_OVER_SCENE )


func _on_win_game():
	# Wait a few seconds before we actually do this..
	yield( get_tree().create_timer(3), "timeout" )
	GLOBAL.change_scene( GLOBAL.VICTORY_SCENE )






func _on_Lava_lava_level(y_pos, rise_rate):
	if !GLOBAL.player:
		return
	var distance = abs(y_pos - GLOBAL.player.position.y)
	var time = distance / rise_rate
	GLOBAL.lava_time = time
	if time > 1:
		$UI/LavaTime.text = str(time).pad_decimals(1)+"s"
	$UI/LavaTime.visible = time > 1








func _on_Trigger_Gem_door_closed():
	self.game_on = true
