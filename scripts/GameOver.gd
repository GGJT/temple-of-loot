extends Control


func show_score():
	$PointsEarned.text = str(GLOBAL.score)+" POINTS"
	
	var mins = int(GLOBAL.game_time) / 60
	var secs = int(GLOBAL.game_time) % 60
	
	$GameTime.text = "%s:%s" % [str(mins).pad_zeros(2), str(secs).pad_zeros(2)]


func _ready():
	$ToMenu.grab_focus()
	show_score()
	$gameovertrack.play()
	$gameovertrack.get_stream().set_loop(false)

	
func _on_ToMenu_pressed():
	GLOBAL.health = GLOBAL.max_health
	GLOBAL.change_scene( GLOBAL.TITLE_SCENE )
	

