extends Label

"""
UI SCORE DISPLAY

The number shown in the display with roll up as score increases
"""


# The ACTUAL score, grepped from GLOBAL signal
var S = 0 setget _set_S

# The value to print in the label
var s = 0 setget _set_s

var tick = 0.05 #time it takes for score to roll up by 1
var t = 0.0		# internal timer, resets @ tick seconds

func _ready():
	GLOBAL.connect("score_changed", self, "_on_score_changed")


# Toggled on/off as needed
func _process(delta):
	t += delta
	if t >= tick:
		t -= tick
		self.s += sign( S-s )


# Callback for GLOBAL score change
func _on_score_changed( to ):
	self.S = to



func _set_S( to ):
	S = to
	# Start rolling if score doesn't match display
	if (S != s):
		set_process(true)


func _set_s( to ):
	s = to
	text = str(s)
	# stop rolling if display matches score
	if (s == S):
		set_process(false)