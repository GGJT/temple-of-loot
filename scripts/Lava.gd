extends Area2D

export (int) var LAVA_SPEED = 60
export (int) var COLS = 4
export (int) var ROWS = 5
export (int) var TILE_WIDTH = 128
export (int) var TILE_HEIGHT = 128
var   			 rising = false
var				 timer

signal lava_level(y_pos, rise_rate)
signal triggerAudio_lava()

func _ready():	
	
	add_to_group("lava")
	$LavaSprite.playing=true

	# Duplicate lava tiles for top row
	var tiles = []
	for i in range(0, COLS-1):
		tiles.append($LavaSprite.duplicate(DUPLICATE_USE_INSTANCING))
		tiles[i].position += Vector2(TILE_WIDTH*(1+i), 0)
	
	# Duplicate will dupe children, so do this separately
	for i in range(0, tiles.size()):	
		$LavaSprite.add_child(tiles[i])
	
	# Duplicate rows	
	for i in range(0, ROWS-1):
		var row = $LavaSprite.duplicate(DUPLICATE_USE_INSTANCING)
		row.position += Vector2(0, TILE_HEIGHT*(1+i))
		add_child(row)

	randomize()
	# Duplicate particles for top row
	for i in range(0, COLS-1):
		var l = $LavaSpurt.duplicate(DUPLICATE_USE_INSTANCING)
		add_child(l)
		l.position += Vector2(TILE_WIDTH*(1+i),0)
		var ap = l.get_node("AnimPlayer2")
		ap.advance(randf()*ap.get_animation("spurt").length)
	

	# Set collision shape to same width as lava
	$CollisionShape2D.shape.extents.x = COLS*TILE_WIDTH/2
	$CollisionShape2D.position.x = (COLS-1)*TILE_WIDTH/2


	# Setup timer to emit lava height signal
	timer = Timer.new()
	timer.wait_time = 0.5
	timer.connect("timeout", self, "_on_timer_timeout") 
	add_child(timer)
	


func _process(delta):
	if(rising && !GLOBAL.god_mode):
		position += Vector2(0,-LAVA_SPEED*delta)


func _on_Trigger_Gem_door_closed():
	emit_signal("triggerAudio_lava")
	rising = true
	timer.start() 
	

func _on_timer_timeout():
	emit_signal("lava_level", position.y-TILE_HEIGHT/2, LAVA_SPEED)
