extends Node2D

var chunks = 12

var choices = [
	preload("res://scenes/chunks/Chunk1.tscn"),
	preload("res://scenes/chunks/Chunk2.tscn"),
	preload("res://scenes/chunks/Chunk3.tscn"),
	preload("res://scenes/chunks/Chunk4.tscn"),
	preload("res://scenes/chunks/Chunk5.tscn"),
	preload("res://scenes/chunks/Chunk6.tscn"),
	preload("res://scenes/chunks/Chunk7.tscn"),
	preload("res://scenes/chunks/Chunk8.tscn"),
]

func _ready():
	var y = -32*16
	for i in range( chunks ):
		# Stack randomly-selected chunks on top of each other
		var choice = choices[randi()%choices.size()].instance()
		add_child(choice)
		choice.position.y = y
		# the height of a Chunk is 16 32-pixel tiles...eww I know!
		y -= 32*16
	# Put the cap on top of the tunnel
	var cap = preload("res://scenes/chunks/Cap.tscn").instance()
	add_child(cap)
	cap.position.y = y